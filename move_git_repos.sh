SRC_DIR=/tmp/

FROM=git@github.com
FROM_USER=USER_OR_PROJECT_AT_FROM
TO=git@gitlab.com
TO_USER=USER_OR_PROJECT_AT_TO

declare -a PROJECTS=(
    "project_1"
    "project_2"
    "project_3"
)

for PROJECT in "${PROJECTS[@]}"
do
    cd ${SRC_DIR}
    git clone --mirror ssh://${FROM}/${FROM_USER}/${PROJECT}
    cd ${PROJECT}.git
    git remote add gitlab ssh://${TO}/${TO_USER}/${PROJECT}.git
    git push gitlab --mirror
done

